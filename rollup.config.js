import pkg from "./package.json"
import ts from "@rollup/plugin-typescript"
import watch from "rollup-plugin-watch"
import shebang from "rollup-plugin-add-shebang"

export default {
  input: "src/index.ts",
  output: {
    file: "exec.js",
    format: "cjs",
  },
  plugins: [
    ts({outputToFilesystem: false}),
    watch({dir: "src"}),
    shebang({
      include: "exec.js",
      shebang: "#!/usr/bin/env node"
    })
  ],
  external: ["path", "fs", ...Object.keys(pkg.dependencies)],
}
