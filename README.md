# Boil

Configurable, Syncable, and Sharable boilerplate NPM project generator.

## Installing

``npm i -g @jonoaugustine/boil``

---

- [Boil](#boil)
  - [Installing](#installing)
  - [Getting Started](#getting-started)
    - [Creating A Plate](#creating-a-plate)
    - [Loading a New Project from a Plate](#loading-a-new-project-from-a-plate)
  - [Setting Template Defaults](#setting-template-defaults)
  - [Creating an Account](#creating-an-account)
  - [Config (WIP)](#config-wip)
  - [Roadmap](#roadmap)

---

## Getting Started

### Creating A Plate

Boil can be used with your own configurations (called `Plates`) and soon with any public Plate as well!

Let's get started by making a new `Plate`. First, let's ask Boil to make us a
template file with `template` or `T`:

```nolang
$ boil template
> Template file created
```

Boil will create a new file called `.template.boil.json`, open up your
favorite text editor and take a look at it. Without any [defaults](#setting-defaults) set, it should
look like a normal `package.json` with some fields missing:

```json
{
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "foo": "bar"
  },
  "author": "",
  "license": "ISC",
  "dependencies": {},
  "devDependencies": {}
}
```

Edit this file to your liking and then return to the command line.

Next we'll tell boil to ingest the template and it will ask for some setup
information. You can optionally pass in your own file, but it will look for
`.template.boil.json` by default. Using `read` or `r`

```nolang
$ boil read [file/path]
? Plate name: foo // The name should not have any spaces! use - or _
? Tags (separate by comma ','): bar, barrius mcbar, barring
? Make package public (anyone can see and download it)? y
? Delete Template? y
```

And that's it! You you just created your first Plate, which will be saved
locally and to the Boil database ([if you are logged in](#creating-an-account)).

### Loading a New Project from a Plate

Now that we have a Plate to work with, let's actually use it to make a new
boilerplate project!

Using a Plate is ezpz:

```nolang
$ boil a [plate_name]
```

If you want to use a foreign plate (one saved to the Boil servers) you just need
to give the author's username along with the plate name:

```nolang
$ boil a <plate_name>@<user_name>
```

If you leave out the `[plate-name]`, boil will search your local Plates
and let you choose from a list:

```nolang
$ boil a
? What Plate would you like to use?
> PlateA
  Plate-B
  Blate
  ...
```

Boil will make a new directory and setup the project `package.json`,
as well as an `index.js` if a `main` is specified. You can also set the name
of the folder to be created like so:

```nolang
$ boil a [plate_name] [folder_name]
```

## Setting Template Defaults

One of the best parts of Boiling is setting defaults to pre-fill in your templates!
Lets setup our defaults:

First let's generate a template like before:

```nolang
$ boil template
```

Edit the template file to your liking and then use the set option to save the
template as your new defaults:

```nolang
$ boil defaults set
```

Defaults are used to pre-fill generated templates each time you use
`boil template` so you can skip even more repetition!

---

## Creating an Account

Signing up for a Boil account gives you access to saving plates with the Boil API.
The syntax is very straight-forward:

```nolang
$ boil signup <email> <password>
```

Logging in works similarly, but allows for `username` login as well as `email` :

```nolang
$ boil login <email> <password>
```

Both login and signup will save credentials and user data locally for use
in other commands.

---

## Downloading Plates (Pull)

A major feature of Boil is the ability to use your (and other's) Plates wherever
you are. The syntax for this is pretty simple:

```nolang
$ boil pull <plate_name>
```

### Pulling Public Plates

When only the `plate_name` is provided, `pull` will look for a plate in the Boil
servers under *your* name. If you wanted to pull a plate made by someone else,
be sure to include their username:

```nolang
$ boil pull <plate_name> <user_name>
```

---

## Uploading and Publishing Plates (Push)

Pulling Plates would be pretty useless without the ability to upload them huh...
Using the `push` command will save your plate to the Boil servers:

```nolang
$ boil push [plate_name]
```

If the local plate has an id, it will try to update the remote instance, otherwise
the plate will be uploaded as new.

---

## Config (WIP)

Boil save you data in config files at the root directory. There are three config files:
`plates.boil.json`, `user.boil.json`, and `defaults.boil.json`.

You can access the config files through boil with the `config` command.

```nolang
$ boil config
? What would you like to do?
> delete all configs
```

---

## Roadmap

- API interaction commands
  - `push` for uploading or updating a plate
  - `pull` for downloading a plate
  - `fetch` for downloading all of a user's plates
- File structure reading
  - ingesting project structures to save for plate project generation
- Automatic uploads of new plates <sup>Tentative</sup>
  - auto-sync: on/off config