async function runCommand(...args) {
  process.argv = ["node", "boil", ...args]
  return require("../exec")
}

module.exports = runCommand