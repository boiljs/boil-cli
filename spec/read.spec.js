const runner = require('./util')
const fs = require("fs-extra")

describe("read", () => {
  let origArgv
  beforeEach(() => {
    jest.resetModules()
    jest.mock("fs-extra")
    origArgv = process.argv
  })

  afterEach(() => {
    jest.resetAllMocks()
    process.argv = origArgv
  })

  it("should run read command", async () => {
    const consoleSpy = jest.spyOn(console, "log")
    await runner("read")
    expect(consoleSpy).toBeCalled()
  })
})