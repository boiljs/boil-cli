import yargs from "yargs"
import cmd from "./commands"


/** Main function */
(async () => {
  yargs
    .scriptName("boil")
    .command("*", "default command", {}, (args) => {
      if (args._.length === 0)
        console.log("Boil", "3.0.0", "\ntry $boil --help")
      else
        console.log(
          `No command by the name of "${ args._[0] }"\ntry $boil --help`,
        )
    })
    .command(cmd.a)
    .command(cmd.defaults)
    .command(cmd.login)
    .command(cmd.read)
    .command(cmd.signup)
    .command(cmd.template)
    .command(cmd.pull)
    .command(cmd.push)
    .help().argv
})().catch((e) => console.log(e))