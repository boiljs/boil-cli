import { ArgumentsCamelCase, CommandModule } from "yargs"
import { promptPlate } from "./inputUtil"
import api from "../api"
import files from "../files"


const command: CommandModule = {
  command: "push [platename]",
  aliases: ["upload"],
  describe:
    "Push a plate update to Boil API." +
    " Must be logged in ($ boil login)" +
    " to upload a plate to the API use $ boil upload <platename>",
  builder: {
    plate: {
      describe: "The name of the plate to push",
      demandOption: false,
      type: "string",
    },
  },
  handler: async ({ platename }: ArgumentsCamelCase<{ platename: string }>) => {
    const plate = platename ? files.plates.get(platename) : await promptPlate()

    if (!plate)
      return console.log(`No Plate with name "${ platename }" was found.`)

    const response = await api.plates[plate._id ? "patch" : "upload"](plate)

    if (!response) return console.error("Failed to push plate")

    const { payload, meta } = response

    if (!payload) console.error("Failed to push plate\n", meta?.error || ",")

    if (meta) console.log("Plate updated")
    else console.log("Plate uploaded")

    if (!files.plates.write(payload)) {
      console.error("Failed to write plate to file")
    }
  },
}

export default command
