import { ArgumentsCamelCase, CommandModule } from "yargs"
import { join } from "path"
import { createFolderLocal, writeLocal } from "../files/util.file"
import { promptPlate } from "./inputUtil"
import { Plate } from "../models"
import { type as FolderType } from "../models/Plate/Folder"
import api from "../api"


/** Creates all files and folders from the given parent folder */
const buildFileStructure = (folder: FolderType, path: string): void => {
  // create files
  folder.files?.forEach(({ name, body }) => {
    if (writeLocal(join(path, name), body || ""))
      console.log(`File "${ name }" written to ${ path }`)
    else
      console.error(`Failed to write file "${ name }" to ${ path }`)
  })
  // create folders
  folder.folders?.forEach(f => {
    const subpath = join(path, f.name)
    if (createFolderLocal(subpath))
      buildFileStructure(f, subpath)
    else
      console.error(`Failed to write folder "${ f.name }"`)
  })
}

const command: CommandModule = {
  command: ["a [plate] [folder]"],
  aliases: "A",
  describe:
    "Loads a new project from a user plate. " +
    "Will build the project in a folder named after the plate unless specified.",
  builder: {
    folder: {
      default: null,
      describe: "Folder path to build the new project.",
      demandOption: false,
      type: "string",
    },
    plate: {
      default: null,
      describe:
        "The plate to boil. To use foreign (online) plates," +
        " use the format <plate_name>@<user_name>",
      demandOption: true,
      type: "string",
    },
  },
  handler: async (args: ArgumentsCamelCase<{ plate: string; folder: string }>) => {
    let plate: Plate.type

    // If no platename given, search local plates
    if (!args.plate) {
      const p = await promptPlate()
      if (!p) return
      plate = p
    }
    // If foreign platename given, check API
    else if (/.+@.+/i.test(args.plate)) {
      const [platename, username] = args.plate.split("@")

      const search = await api.plates.searchPlate(platename, username)
      if (search?.success) {
        plate = search.payload[0]
      } else {
        return console.error(
          `Failed to get ${ platename }@${ username }`,
          search?.meta?.error || "",
        )
      }
    }

    // Create root folder
    args.folder = args.folder || plate.fileStructure?.name || plate.name
    if (!createFolderLocal(args.folder))
      return console.error("Failed to create project folder")

    // Create package.json
    if (
      !writeLocal(
        `${ args.folder }/package.json`,
        JSON.stringify(plate, null, 2),
      )
    ) {
      return console.error("Failed to create project package.json")
    }

    // Construct fileStructure tree when it gets implemented on client side
    if (plate.fileStructure) buildFileStructure(plate.fileStructure, args.folder)

    // Create entry file
    if (typeof plate.package.main === "string") {
      const paths = plate.package.main.split("/")
      // check if subfolder specified
      let folder = "."
      if (paths.length > 1) {
        folder = paths.slice(0, paths.length - 1).join("/")
        createFolderLocal(folder)
      }

      const file = paths[paths.length - 1]

      writeLocal(
        join(args.folder, folder, file),
        "// Autogenerated boil file\n\nconst main = async () => {};\n\nmain();",
      )
    }

    console.log(
      `Created new project with plate "${ plate.name }" at ${ args.folder }`,
    )
  },
}

export default command
