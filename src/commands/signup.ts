import inq from "inquirer"
import { ArgumentsCamelCase, CommandModule } from "yargs"
import files from "../files"
import { writeCreds } from "../files/user.file"
import api from "../api"


const command: CommandModule = {
  command: "signup [email] [password] [username]",
  aliases: ["register"],
  describe: "Create a new account and set saved login credentials",
  builder: {
    email: {
      describe: "Email to create an account with",
      demandOption: false,
      type: "string",
      alias: "e",
    },
    password: {
      demandOption: false,
      type: "string",
      alias: "p",
    },
    username: {
      describe: "Username to register with. A username is required to push public Plates",
      demandOption: false,
      type: "string",
      alias: "u",
    },
  },
  handler: async ({
    email,
    password,
    username,
  }: ArgumentsCamelCase<{
    email: string
    password: string
    username: string
  }>) => {
    if (!email) {
      const { e } = await inq.prompt([
        {
          type: "input",
          name: "e",
          message: "Email",
        },
      ])
      email = e
    }

    if (!password) {
      const { pw } = await inq.prompt([
        {
          type: "password",
          name: "pw",
          message: "Password",
        },
      ])
      password = pw
    }

    let pwcheck = ""
    let retry = true
    while (pwcheck !== password && retry) {
      pwcheck = (await inq.prompt([
        {
          type: "password",
          name: "pwcheck",
          message: "Retype Password",
        },
      ])).pwcheck
      if (pwcheck !== password) {
        retry = (await inq.prompt([{
          type: "confirm",
          name: "r",
          message: "Password and retype do not match, try again?",
        }])).r
        if (!retry) return
      }

    }

    if (!username) {
      const { u } = await inq.prompt([
        {
          type: "input",
          name: "u",
          message: "Optional Username",
        },
      ])
      username = u || undefined
    }

    const response = await api.users.register(email, username, password)

    if (!response) return console.error("Failed to register user")

    const { payload: user, meta } = response

    if (!user) return console.error("Failed to register user\n", meta?.error || "")

    console.log("User registered")

    if (!writeCreds({ email, username, password }))
      console.error("Failed to write credentials to file.\Try $boil login")

    if (files.users.writeUser(user))
      console.log("Logged in as " + (user.username || user.email))
    else
      console.error("Failed to write credentials to file")

  },
}

export default command
