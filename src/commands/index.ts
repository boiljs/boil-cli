import a from "./a"
import defaults from "./defaults"
import login from "./login"
import read from "./read"
import signup from "./signup"
import template from "./template"
import pull from "./pull"
import push from "./push"

export default {
  a,
  defaults,
  login,
  read,
  signup,
  template,
  pull,
  push,
}
