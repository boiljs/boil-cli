import inq from "inquirer"
import { ArgumentsCamelCase, CommandModule } from "yargs"
import files from "../files"
import api from "../api"


const command: CommandModule = {
  command: "login [email] [password]",
  describe: "Set saved login credentials",
  builder: {
    email: {
      describe: "Account email",
      demandOption: false,
      type: "string",
      alias: "u",
    },
    password: {
      describe: "Account password",
      demandOption: false,
      type: "string",
      alias: "p",
    },
  },
  handler: async ({
    email,
    password,
  }: ArgumentsCamelCase<{ email: string; password: string }>) => {
    if (!email) {
      const { id } = await inq.prompt([
        {
          type: "input",
          name: "id",
          message: "Email",
        },
      ])
      email = id
    }

    if (!password) {
      const { pw } = await inq.prompt([
        {
          type: "password",
          name: "pw",
          message: "Password",
        },
      ])
      password = pw
    }

    const response = await api.users.login(email, password)

    if (!response) return console.error("Failed to login")

    const { payload: user, meta } = response

    if (!user) {
      return console.error(
        "If you do not have an account, use $ boil signup",
        meta?.error || "",
      )
    }

    if (files.users.writeUser(user)) {
      if (files.users.writeCreds({ email: user.email, password })) {
        console.log(`Logged in as ${ user.username || user.email }`)
      } else {
        console.error("The login succeeded, but failed to save credentials to file")
      }
    } else {
      console.error("Failed to write user data to file")
    }
  },
}

export default command
