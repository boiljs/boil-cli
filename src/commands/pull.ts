import { ArgumentsCamelCase, CommandModule } from "yargs"
import db from "../files"
import api from "../api"


const command: CommandModule = {
  command: "pull <platename> [username]",
  aliases: ["get"],
  describe: "Pull a plate from the Boil API and save it locally",
  builder: {
    platename: {
      describe: "The name of the plate",
      demandOption: true,
      type: "string",
    },
    username: {
      describe: "The name of the plate author",
      demandOption: false,
      type: "string",
    },
  },
  handler: async (
    { platename, username }:
      ArgumentsCamelCase<{ platename: string, username?: string }>,
  ) => {
    const author = username
      || db.users.readCreds()?.username
      || db.users.readUser()?.username

    if (!author) return console.error("No author given or not logged in")

    const response = await api.plates.searchPlate(platename, author)

    if (!response) return console.error("Failed to pull")

    const { payload: plates, meta } = response

    if (plates[0]) {
      if (db.plates.write(plates[0])) {
        console.log(`Plate "${ platename }" downloaded and saved`)
      } else console.log(`Failed to write Plate "${ platename }" to file`)
    } else {
      console.error("Boil was unable to find a plate\n", meta?.error || "")
    }
  },
}

export default command
