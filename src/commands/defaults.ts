import { ArgumentsCamelCase, CommandModule } from "yargs"
import { TEMPLATE_DEFAULT_NAME } from "../constants"
import files from "../files"
import { deleteFileLocal, readLocal } from "../files/util.file"
import api from "../api"


const command: CommandModule = {
  command: "defaults [set]",
  aliases: ["d", "D"],
  describe: "View or load new template defaults.",
  builder: {
    set: {
      default: false,
      describe:
        "If set to true, boil will read a template file and set it's contents as the new defaults.",
      demandOption: false,
      type: "boolean",
    },
  },
  handler: async (args: ArgumentsCamelCase<{ set: string }>) => {
    if (args.set) {
      const templateRaw = readLocal(TEMPLATE_DEFAULT_NAME)
      if (!templateRaw) {
        return console.error(`No template found under ./${ TEMPLATE_DEFAULT_NAME }`)
      }

      const template = JSON.parse(templateRaw)
      const updatedUser = await api.users.update({ defaultPackage: template })

      if (updatedUser) {
        console.log("Updated Default Package")
      } else {
        return console.log("Failed to update")
      }

      if (deleteFileLocal(TEMPLATE_DEFAULT_NAME)) {
        console.log("template file deleted")
      }
    } else {
      console.log(
        files.users.readUser()?.defaultPackage
        || "No default package found\ntry $boil defaults set",
      )
    }
  },
}

export default command
