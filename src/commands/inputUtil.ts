import inq from "inquirer"
import files from "../files"
import { Plate } from "../models"


/**
 * Prompt the user for a Plate Name from existing **local** plates
 *
 * @returns The plate selected by the user
 */
export const promptPlate = async (): Promise<Plate.type | null> => {
  if (files.plates.getAll() && Object.keys(files.plates.getAll()).length > 0) {
    const { plate: platename } = await inq.prompt([
      {
        type: "list",
        name: "plate",
        choices: Object.keys(files.plates.getAll()),
      },
    ])
    return files.plates.get(platename)
  } else {
    console.log("No plates available. Please create a plate. try $ boil --help")
    return null
  }
}
