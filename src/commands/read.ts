import inq from "inquirer"
import { ArgumentsCamelCase, CommandModule } from "yargs"
import { Plate } from "../models"
import db from "../files"
import { TEMPLATE_DEFAULT_NAME } from "../constants"
import { deleteFileLocal, readLocal } from "../files/util.file"


const readTemplate = (path: string) => {
  const f = readLocal(path)
  return f ? JSON.parse(f) : undefined
}

const command: CommandModule = {
  command: "read [file]",
  aliases: ["r", "R"],
  describe:
    "Ingest a template. You can pass in your own file, " +
    "but it will look for .template.boil.json by default.",
  builder: {
    file: {
      default: TEMPLATE_DEFAULT_NAME,
      describe: "File path of the template to read",
      demandOption: false,
      type: "string",
    },
  },
  handler: async ({ file }: ArgumentsCamelCase<{ file: string }>) => {
    const template = readTemplate(file)

    if (!template)
      return console.log("Failed to read template. Use $ boil read --help")

    const result = await inq.prompt([
      {
        type: "input",
        name: "name",
        message: "Plate name",
        validate: async (input) => {
          if (!/^[a-z][0-9a-z\-_]{0,50}$/i.test(input)) {
            return `Plate name "${ input }" is invalid.\nPlate names must be 1-50 characters and contain only:\nletters, numbers, hypens (-), and underscores (_)`
          } else if (db.plates.get(input)) {
            return `Plate name ${ input } already in use`
          } else return true
        },
      },
      {
        type: "input",
        name: "tags",
        message: "Tags (separate by comma ',')",
        validate: (input) =>
          input.match(/^[0-9_a-z\-,\s]{0,200}$/i)
            ? true
            : "Please use only letters, numbers, hypens (-), and underscores (_) and keep the tag list under 200 characters",
      },
      {
        type: "confirm",
        name: "public",
        message: "Make plate public",
      },
      {
        type: "confirm",
        name: "delete",
        message: "Delete template file",
      },
    ])

    if (result.delete && !deleteFileLocal(file)) {
      console.log(
        "Failed to delete template. I will still try to complete the command.",
      )
    }

    const plate: Plate.type = {
      userID: db.users.readUser()._id || "0",
      name: result.name,
      package: template,
      public: result.public,
      tags:
        result.tags
          .split(/,+/)
          .map((t: string) => t.trim())
          .filter((t: string) => t.length > 0) || [],
    }

    if (db.plates.write(plate)) console.log("New plate added!")
    else console.log("Failed to save new plate :(")
  },
}

export default command
