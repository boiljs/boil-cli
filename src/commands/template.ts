import { CommandModule } from "yargs"
import db from "../files"
import { TEMPLATE_DEFAULT_NAME } from "../constants"
import { writeLocal } from "../files/util.file"


const template = {
  version: "1.0.0",
  description: "",
  main: "index.js",
  scripts: {
    start: "node ./index.js",
  },
  author: "",
  license: "ISC",
  dependencies: {},
  devDependencies: {},
}

const generateTemplate = async () => {
  writeLocal(
    TEMPLATE_DEFAULT_NAME,
    JSON.stringify(
      {
        ...template,
        ...db.users.readUser().defaultPackage,
      },
      null,
      2,
    ),
  )

  console.log(`Template file created at ${ TEMPLATE_DEFAULT_NAME }`)
}

const command: CommandModule = {
  /** Template command invocation */
  command: "template",
  aliases: ["t", "T"],

  /** Template command description */
  describe:
    "Creates a new file called .template.boil.json to be" +
    " edited and used by boil to make a new plate",
  builder: {},
  handler: () => generateTemplate(),
}

export default command
