import joi from "joi"
import * as Name from "../Plate/Name"
import * as Package from "../Plate/Package"

const schema = joi.object({
  username: Name.schema.optional(),
  email: joi
    .string()
    .email()
    .required()
    .alter({ update: (s) => s.optional() }),
  defaultPackage: Package.schema.optional(),
})

const UserUpdatePartial = schema.tailor("update")

enum UserRole {
  GENERIC,
  CONTENT_MANAGER,
  ADMIN,
}

type UserType = {
  _id?: string
  email: string
  username?: string
  defaultPackage?: Package.type
  role?: UserRole
}

export {
  schema,
  UserUpdatePartial as updatePartial,
  UserType as type,
  UserRole as Roles,
}
