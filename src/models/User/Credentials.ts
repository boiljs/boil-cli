type Credentials = {
  email: string
  username?: string
  password: string
}

export { Credentials as type }
