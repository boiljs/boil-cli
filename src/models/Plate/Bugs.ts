import joi from "joi"

const schema = joi.object({
  url: joi.string().uri(),
  email: joi.string().email(),
})

type BugsType = { url?: string; email?: string }

export { BugsType as type, schema }
