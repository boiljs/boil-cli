import joi from "joi"

const schema = joi.object({
  name: joi.string().optional(),
  url: joi.string().uri().optional(),
})

type RepositoryType = { name?: string; url?: string }

export { schema, RepositoryType as type }
