import joi from "joi"

/** * Joi schema for the package.author property. */
const schema = joi.alternatives().try(
  joi.string().max(214).allow(""),
  joi.object({
    name: joi.string().max(63),
    email: joi.string().email(),
    url: joi.string().uri(),
  }),
)

/** Typing for package.author property. */
type AuthorType =
  | string
  | {
      name?: string
      email?: string
      url?: string
    }

export { AuthorType as type, schema }
