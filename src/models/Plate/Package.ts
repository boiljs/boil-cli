import joi from "joi"
import * as Author from "./Author"
import * as Bugs from "./Bugs"
import * as Dependency from "./Dependency"
import * as Funding from "./Funding"
import * as Repository from "./Repository"

const schema = joi.object({
  name: joi.string().max(214).optional().allow(""),
  description: joi.string().optional().max(1024).allow(""),
  version: joi.string().max(100).optional().allow(""),
  author: Author.schema.optional(),
  main: joi.string().max(255).optional().allow(""),
  repository: Repository.schema.optional(),
  license: joi.string().max(64).optional().allow(""),
  bugs: Bugs.schema.optional(),
  homepage: joi.string().uri().allow(""),
  dependencies: Dependency.schema.optional(),
  devDependencies: Dependency.schema.optional(),
  funding: joi
    .alternatives()
    .try(
      Funding,
      joi.string().uri(),
      joi.array().items(Funding, joi.string().uri()),
    ),
  scripts: joi
    .object()
    .optional()
    .custom((value) => {
      for (const k in value) {
        if (!/^[0-9a-z\-\.]+$/i.test(k)) throw new Error("invalid script name")
        if (typeof value[k] !== "string" || value[k].length > 1024)
          throw new Error("invalid script value")
      }
      return value
    }),
})

type PackageType = {
  name?: string
  description?: string
  version?: string
  author?: Author.type
  main?: string
  repository?: Repository.type
  license: string
  bugs?: Bugs.type
  homepage: string
  dependencies?: Record<string, string>
  devDependencies?: Record<string, string>
  funding?: Funding.type | string | Array<Funding.type | string>
  scripts?: Record<string, string>
}

export { schema, PackageType as type }
