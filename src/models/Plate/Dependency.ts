import joi from "joi"

const schema = joi.object().custom((value) => {
  for (const k in value) {
    if (typeof value[k] !== "string" || value[k].length > 1024)
      throw new Error("invalid dependency value")
  }
  return value
})

type DependencyType = {
  [key: symbol]: string
}

export { schema, DependencyType as type }
