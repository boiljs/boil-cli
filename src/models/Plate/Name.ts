import joi from "joi"

const schema = joi
  .string()
  .min(3)
  .max(33)
  .pattern(/^[a-z][a-z0-9_\-]+$/i)
  .optional()

export { schema }
