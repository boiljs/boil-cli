import joi from "joi"

const schema = joi.object({
  type: joi.string().max(214),
  url: joi.string().uri(),
})

type FundingType = { type?: string; url?: string }

export { schema, FundingType as type }
