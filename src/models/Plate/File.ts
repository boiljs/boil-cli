import joi from "joi"

const schema = joi.object({
  name: joi.string().max(1024).required(),
  body: joi.string().max(20420).optional(),
})

type FileType = {
  name: string
  body?: string
}

export { schema, FileType as type }
