import joi from "joi"
import * as Folder from "./Folder"
import * as Name from "./Name"
import * as Package from "./Package"

const schema = joi.object({
  name: Name.schema.required().alter({ update: (s) => s.optional() }),
  public: joi.boolean().default(false),
  tags: joi.array().items(
    joi
      .string()
      .min(3)
      .max(20)
      .pattern(/^[a-z0-9][\s+a-z0-9\-]+$/i),
  ),
  package: Package.schema.required().alter({ update: (s) => s.optional() }),
  fileStructure: Folder.schema.optional(),
})

const PlateUpdatePartial = schema.tailor("update")

type PlateType = {
  _id?: string
  userID: string
  name: string
  public?: boolean
  tags?: Array<string>
  package?: Package.type
  fileStructure?: Folder.type
}

export { schema, PlateType as type, PlateUpdatePartial as updatePartial }
