import joi from "joi"
import * as File from "./File"


const schema = joi
  .object({
    name: joi.string().max(1024).required(),
    files: joi.array().items(File.schema).optional(),
    folders: joi.array().items(joi.link("#folder")).optional(),
  })
  .id("folder")

type FolderType = {
  name: string
  files?: Array<File.type>
  folders?: Array<FolderType>
}

export { schema, FolderType as type }
