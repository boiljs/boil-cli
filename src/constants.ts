require("dotenv").config()

/**
 * Deafult name of the generated template file.
 */
export const TEMPLATE_DEFAULT_NAME = ".template.boil.json"

export const API_URI =
  (() => {
    if (process.env.NODE_ENV == "development") {
      console.log("Running in development mode")
      return "http://127.0.0.1:6901/api/v2"
    } else return "https://boil-server.onrender.com/api/v2"
  })()
