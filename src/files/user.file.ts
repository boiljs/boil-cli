import { readHome, writeHome } from "./util.file"
import { User, Credentials } from "../models"
import paths from "./paths"


const _cache: { user: User.type; creds: Credentials.type } = {
  user: null,
  creds: null,
}

function writeCreds(creds: Credentials.type): boolean {
  _cache.creds = creds
  let written = false
  let tries = 0
  do {
    written = writeHome(paths.creds, JSON.stringify(creds))
  } while (!written || tries++ < 5)
  return written
}

function writeUser(user: User.type): boolean {
  _cache.user = user
  let written = false
  let tries = 0
  do {
    written = writeHome(paths.user, JSON.stringify(user))
  } while (!written || tries++ < 5)
  return written
}

function readUser(): User.type {
  if (!_cache.user) {
    const raw = readHome(paths.user)
    if (raw) _cache.user = JSON.parse(raw)
  }
  return _cache.user
}

function readCreds(): Credentials.type {
  if (!_cache.creds) {
    const raw = readHome(paths.creds)
    if (raw) _cache.creds = JSON.parse(raw)
  }
  return _cache.creds
}

export { writeCreds, writeUser, readUser, readCreds }
