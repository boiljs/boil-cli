/*
  A collection of filel utilities to abstract read/write ops
  in local and root paths
 */

import fs from "fs-extra"
import { join } from "path"

/**
 * Reads a file relative to the invocation directory.
 *
 * @param {string} path - local path to file
 * @returns The file contents or null if the read failed.
 *
 * @since 0.2.0
 * @author JonoAugustine
 */
export const readLocal = (path: string): string | null => {
  const fPath = join(process.cwd(), path)
  try {
    return fs.readFileSync(fPath, "utf8")
  } catch (error) {
    return null
  }
}

/**
 * Writes the given content to a file at the
 * given path relative to the invocation directory.
 *
 * @param {string} path - path to local write destination
 * @param {string} content - The content to write to file
 * @returns {boolean} `true` if the file was written.
 *
 * @since 0.2.0
 * @author JonoAugustine
 */
export const writeLocal = (path: string, content: string): boolean => {
  try {
    fs.writeFileSync(join(process.cwd(), path), content, "utf8")
  } catch (_) {
    return false
  }
  return true
}

/**
 * Deletes a file relative to the invocation directory.
 *
 * @param {string} path - invocation path to file
 * @returns {boolean} `true` if the file was deleted
 *
 * @since 0.2.0
 * @author JonoAugustine
 */
export const deleteFileLocal = (path: string): boolean => {
  try {
    fs.unlinkSync(join(process.cwd(), path))
  } catch (error) {
    return false
  }
  return true
}

/**
 * @param {string} path - The relative path of the folder to create
 * @returns
 *
 * @since 0.2.0
 * @author JonoAugustine
 */
export const createFolderLocal = (path: string): boolean => {
  try {
    fs.mkdirSync(path, { recursive: true })
  } catch (_) {
    return false
  }
  return true
}

/**
 * Reads a file relative to the home (install) directory.
 *
 * @param {string} path - home (install) path to file
 * @returns The file contents or null if the read failed.
 *
 * @since 0.2.0
 * @author JonoAugustine
 */
export const readHome = (path: string): string => {
  const fPath = join(__dirname, "..", path)
  try {
    return fs.readFileSync(fPath, "utf8")
  } catch (error) {
    return null
  }
}

/**
 * Writes the given content to a file at the
 * given path relative to the home (install) directory.
 *
 * @param {string} path - path to home (install) write destination
 * @param {string} content - The content to write to file
 * @returns {boolean} `true` if the file was written.
 *
 * @since 0.2.0
 * @author JonoAugustine
 */
export const writeHome = (path: string, content: string): boolean => {
  try {
    fs.writeFileSync(join(__dirname, "..", path), content, "utf8")
  } catch (_) {
    return false
  }
  return true
}

/**
 * Deletes a file relative to the home (install) directory.
 *
 * @param {string} path - home (install) path to file
 * @returns `true` if the file was deleted
 *
 * @since 0.2.0
 * @author JonoAugustine
 */
export const deleteFileHome = (path: string): boolean => {
  try {
    fs.unlinkSync(join(__dirname, "..", path))
  } catch (error) {
    return false
  }
  return true
}
