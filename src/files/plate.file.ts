import { readHome, writeHome } from "./util.file"
import { Plate } from "../models"
import paths from "./paths"


type PlateMap = Record<string, Plate.type>

const _cache: { plates: PlateMap } = { plates: null }

/**
 * Add a sinlge plate to local store
 * @param plate
 */
function write(plate: Plate.type): boolean {
  const plates = getAll()
  plates[plate.name] = plate
  _cache.plates[plate.name] = plate

  let written = false
  let tries = 0
  do {
    written = writeHome(paths.plates, JSON.stringify(plates))
  } while (!written || tries++ < 5)
  return written
}

function deletePlate(name: string) {
  const plates = getAll()
  delete plates[name]
  delete _cache.plates[name]
  return writeHome(paths.plates, JSON.stringify(plates))
}

function get(name: string): Plate.type {
  return getAll()[name]
}

function getAll(): PlateMap {
  if (!_cache.plates) {
    const raw = readHome(paths.plates)
    _cache.plates = raw ? JSON.parse(raw) : {}
  }
  return _cache.plates
}

export { getAll, get, deletePlate, write }
