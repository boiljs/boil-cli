import axios from "axios"
import { API_URI } from "../constants"
import * as users from "./user.file"
import * as plates from "./plate.file"
import * as util from "./util.file"


axios.defaults.baseURL = API_URI
axios.defaults.headers.common["Content-Type"] = "application/json"

export default {
  users,
  plates,
  ...util,
}
