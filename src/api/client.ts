import axios from "axios"
import { API_URI } from "../constants"


const client = axios.create({
  baseURL: API_URI,
  httpsAgent: "boil-cli/3",
  headers: {
    "Content-Type": "application/json",
  },
})

export default client