import { Packet } from "packetier"
import { Plate } from "../models"
import client from "./client"
import { readCreds } from "../files/user.file"


async function searchPlate(
  name?: string,
  username?: string,
  tags?: string[],
  isPublic?: boolean,
): Promise<Packet<Plate.type[]>> {
  try {
    const { data } = await client.get<Packet<Plate.type[]>>("/plates", {
      headers: { ...readCreds() },
      params: { name, username, tags, public: isPublic },
    })
    return data
  } catch (e) {
    return e?.response?.data || null
  }
}

/**
 * Send a plate to the API to update an existing plate
 * @param plate
 */
async function patch(plate: Plate.type): Promise<Packet<Plate.type>> {
  try {
    const { data } = await client.patch(`/plates/${ plate._id }`, plate, { headers: readCreds() })
    return data
  } catch (e) {
    return e?.response?.data || null
  }
}

/**
 * Send a plate to the API to be saved
 *  @param {Plate.type} plate
 */
async function upload(plate: Plate.type): Promise<Packet<Plate.type>> {
  try {
    const { data } = await client.post("/plates", plate, { headers: readCreds() })
    return data
  } catch (e) {
    return e?.response?.data || null
  }
}

export { searchPlate, patch, upload }