import { Packet } from "packetier"
import { User } from "../models"
import client from "./client"
import { readCreds, readUser, writeCreds } from "../files/user.file"


/** Registers a new user. Returns a Packet with the created user */
async function register(
  email: string,
  username: string,
  password: string,
): Promise<Packet<User.type>> {
  try {
    const { data } = await client.post<Packet<User.type>>(`/users/register`, {
      email,
      username,
      password,
    })
    return data
  } catch (e) {
    return e?.response?.data || null
  }
}

/**
 * Logs in user, saves the new credentials on success
 */
async function login(
  email: string,
  password: string,
): Promise<Packet<User.type>> {
  try {
    const { data } = await client.post<Packet<User.type>>(`/users/login`, {
      email,
      password,
    })

    if (!writeCreds({ email, password })) console.log("failed to write credentials")

    return data
  } catch (e) {
    return e?.response?.data || null
  }
}

async function update(partial: Partial<User.type>): Promise<Packet<User.type>> {
  const user = readUser()
  if (!user) throw new Error("no user")
  try {
    const { data } = await client.patch<Packet<User.type>>(
      `/users/${ user._id }`,
      partial,
      { headers: readCreds() },
    )
    return data
  } catch (error) {
    return error?.response?.data || null
  }
}

// TODO user delete

export { register, login, update }