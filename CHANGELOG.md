Boil CLI ChangeLog

# [2.2.0](https://gitlab.com/boiljs/boil-cli/compare/v2.1.0...v2.2.0) (2022-05-19)


### breaking

* change identity to email in login command ([e5d0956](https://gitlab.com/boiljs/boil-cli/commit/e5d0956b07d4fff43082a1cb128b4c0e0542bbd8))

### build

* fix release rules and changelog updating ([079049b](https://gitlab.com/boiljs/boil-cli/commit/079049b6691b2bc0fd14e9bf4c30222adcb94513))
* update release config ([5504b4f](https://gitlab.com/boiljs/boil-cli/commit/5504b4fd930de61056ee715dd3f2c42156bca2a8))
* remove old bumper ([e1f07c7](https://gitlab.com/boiljs/boil-cli/commit/e1f07c7d8809e4fdd4ca452d57c24b54309a80c4))

### change

* add exec to gitignore ([3c4053d](https://gitlab.com/boiljs/boil-cli/commit/3c4053d8028bd96d6e47b31d8e7045e4f48d46a6))
* clarify error message in defaults.ts ([164aa9c](https://gitlab.com/boiljs/boil-cli/commit/164aa9c10f13cadd06b9e8e263da94edfeb74f5a))
* consolidate exec and bundle file ([6de0028](https://gitlab.com/boiljs/boil-cli/commit/6de002821f69f962869e74d9b17e489cd5a86bfb))
* improve code readability and composition ([7109d74](https://gitlab.com/boiljs/boil-cli/commit/7109d74db89b45945ac2ff069c7eb12418f01e6c))
* move client to api module ([27656f1](https://gitlab.com/boiljs/boil-cli/commit/27656f1dd12718656e8277d1bf4fb5fea812062e))
* separate file and api functions into their own modules ([b4889b7](https://gitlab.com/boiljs/boil-cli/commit/b4889b7243ed6a2cfc3daaba5c03a9e0408d73f5))
* version update ([54c9b02](https://gitlab.com/boiljs/boil-cli/commit/54c9b027c914816c542d5ac8d41a590295fde198))

### chore

* cleanup code a bit ([593f461](https://gitlab.com/boiljs/boil-cli/commit/593f461fc063c04caed2b56a2f6eeadc56ecfb21))
* migrate to v2 API and refactor code structure ([91f5995](https://gitlab.com/boiljs/boil-cli/commit/91f5995c73e0e9a60d5784796de463b3e948bf4f))
* udpate backend URI ([ae6dd8b](https://gitlab.com/boiljs/boil-cli/commit/ae6dd8b4a432b6bd7dde883cc2515625e6969176))

### Docs

* fix typo in readme ([130cc8e](https://gitlab.com/boiljs/boil-cli/commit/130cc8eff86a04971773a9a061e03b4035e8ec36))

### fix

* handle plate search array return ([d23304a](https://gitlab.com/boiljs/boil-cli/commit/d23304a15f1c3bcc055f2a6ad8b0de042a38a1af))
* write pushed plate to file ([fadb5db](https://gitlab.com/boiljs/boil-cli/commit/fadb5db3899ad885c7092096e07b4e34f2429867))

### new

* build plate file structures ([4ac7c18](https://gitlab.com/boiljs/boil-cli/commit/4ac7c188c0e681b0a29c867bc1386a2d3f4f3130))

### Refactor

* Move to TS ([16b3a6d](https://gitlab.com/boiljs/boil-cli/commit/16b3a6da8947f7f0d73d9d41819e75c41a43c8a2))

### test

* start work on tests ([1590f94](https://gitlab.com/boiljs/boil-cli/commit/1590f9465ae1a117ccb44fe5254c2133d962f05e))
